#include <rc_main/main.h>
#include <rc_task/rcTaskManager.h>
#include "main.hpp"
#include <signal.h>

bool ctrl_c_pressed=false;
void ctrlc(int) {
    ctrl_c_pressed=true;
    std::cout<<"Exit"<<std::endl;
}
int main(int argc, char **argv) {
    signal(SIGINT, ctrlc);

    if (!ParseAndCheckCommandLine(argc, argv)) {
        return 0;
    }
    slog::info << RC_STRING_SYSTEM_START << slog::endl;
    try {
        cv::FileStorage fsread;
        if (FLAGS_c.empty()) {
            if (!fsread.open("config.xml", cv::FileStorage::READ)) {
                slog::err << "Can not open config.xml" << slog::endl;
                generateDefaultConfig();
                exit(-1);
            }
        } else {
            if (!fsread.open(FLAGS_c, cv::FileStorage::READ)) {
                slog::err << "No such file to open" << slog::endl;
                exit(-1);
            }
        }
        /** 获取计算设备类型 CPU 或 FPGA **/
        const std::string device = (std::string) fsread["device"];

        /** 获取摄像机配置 **/
        cv::FileNode camera_node = fsread["camera"];
        //需要启动的相机消息管道名称
        std::string camera_message_queue = (std::string) camera_node["message_queue"];
        //需要启动的相机类型
        //CAMERA_TYPE CAMERA_DEVICE_TYPE = (CAMERA_TYPE) camera_node["type"];
        const int CAMERA_DEVICE = (int) camera_node["type"];
        //相机类型为本地视频的时候需要指定文件路径
        const std::string CAMERA_VEDIO_FILE = (std::string) camera_node["filepath"];

        /** 获取运动控制器配置 **/
        cv::FileNode srial_node = fsread["serial"];
        //串口地址
        const std::string SEERIAL_DEVICE = (std::string) srial_node["path"];
        int SEERIAL_DEVICE_FREQ = (int) srial_node["freq"];


        /** 获取计算服务器配置 **/
        cv::FileNode center_node = fsread["server"];
        //控制中心地址
        const std::string REMOTE_HOST = (std::string) center_node["ipaddress"];
        //控制中心端口号
        const int REMOTE_PORT = (int) center_node["port"];

        /** WEB服务器配置 **/
        cv::FileNode httpd_node = fsread["httpd"];
        //本地绑定地址
        int HTTPD_PORT = (int) httpd_node["port"];

        /** 获取WEBSOCKET服务器配置 **/
        cv::FileNode websocket_node = fsread["websocket"];
        //本地绑定地址
        int WEBSOCKET_PORT = (int) websocket_node["port"];


        /** 获取模型文件配置 **/
        cv::FileNode model_node = fsread["model"];
        const std::string license_plate_recognition_barrier = model_node["license-plate-recognition-barrier"];
        const std::string vehicle_attributes_recognition_barrier = model_node["vehicle-attributes-recognition-barrier"];
        const std::string vehicle_license_plate_detection_barrier = model_node["vehicle-license-plate-detection-barrier"];
        const std::string road_segmentation_adas = model_node["road-segmentation-adas"];
        const std::string single_image_super_resolution = model_node["single-image-super-resolution"];

        /** 获取地图 **/
        cv::FileNode map_node = fsread["map"];
        //地图文件
        const std::string MAPPING = (std::string) map_node["path"];

        /** 获取雷达设备 **/
        cv::FileNode radar_node = fsread["radar"];
        const std::string RADAR_DEVICE = (std::string) radar_node["path"];

        /** 获取陀螺仪设备 **/
        cv::FileNode gyroscope_node = fsread["gyroscope"];
        const std::string GYRO_DEVICE = (std::string) gyroscope_node["path"];
        int GYRO_DEVICE_FREQ = (int) gyroscope_node["fre"];

        /** 获取制动配置 **/
        cv::FileNode gpio_node = fsread["gpio"];
        //制动端口号0，1,2
        int gpio_0 = (int) gpio_node["dist_0"];
        int gpio_1 = (int) gpio_node["dist_1"];
        int gpio_2 = (int) gpio_node["dist_2"];
        //PWM端口号0，1,2
        int pwm_0 = (int) gpio_node["pwm_0"];
        int pwm_1 = (int) gpio_node["pwm_1"];
        int pwm_2 = (int) gpio_node["pwm_2"];

        RC::Task::TaskManager taskmanager((char *) "RoboCar");

        taskmanager.init(CAMERA_DEVICE,//摄像机编号
                         SEERIAL_DEVICE, //控制串口地址
                         SEERIAL_DEVICE_FREQ, //控制串口波特率
                         RADAR_DEVICE,//雷达地址
                         GYRO_DEVICE,//陀螺仪地址
                         GYRO_DEVICE_FREQ,//陀螺仪波特率
                         MAPPING,//地图文件地址
                         HTTPD_PORT,//httpd绑定端口
                         WEBSOCKET_PORT,//websocket绑定端口
                         REMOTE_HOST,//远程服务器地址
                         REMOTE_PORT,//远程服务器端口
                         gpio_0, pwm_0,//制动端口号1和PWM端口号1
                         gpio_1, pwm_1,//制动端口号2和PWM端口号2
                         gpio_2, pwm_2//制动端口号3和PWM端口号3
        );
        taskmanager.start_main_task();
        while (!ctrl_c_pressed) {}
        taskmanager.release();
        getchar();
    } catch (cv::Exception &e) {
        slog::err << "配置加载失败" << slog::endl;
    } catch (boost::exception &e) {
        slog::err << "启动错误" << slog::endl;
    }
    return 0;
}


