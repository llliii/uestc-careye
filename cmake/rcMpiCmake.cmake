message(STATUS "FIND OpenMPI")
find_package(MPI REQUIRED)

message(STATUS "MPI_CXX_HEADER_DIR:${MPI_CXX_HEADER_DIR}")
include_directories(${MPI_CXX_HEADER_DIR})
message(STATUS "MPI_CXX_LIBRARIES:${MPI_CXX_LIBRARIES}")
