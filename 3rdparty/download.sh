#!/usr/bin/env bash
git clone https://github.com/BVLC/caffe.git ./caffe
git clone -b 3.4.1 https://github.com/opencv/opencv.git ./opencv
git clone git://git.videolan.org/vlc.git ./vlc
mkdir  opencv/opencv_build
cd ../opencv_build
cmake ../opencv
make -j4

mkdir caffe_build
cd caffe_build
cmake ../caffe
make -j4
