//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_EC_IMAGE_MSG_H
#define ROBOCAR_EC_IMAGE_MSG_H

#include <rc_message/rc_base_msg.hpp>
#include <map>
#include <string>
#include <opencv2/opencv.hpp>
#include <boost/thread/mutex.hpp>

namespace rccore {
    namespace message {
        class ImageMessage : public BaseMessage<cv::Mat> {
        public:
            explicit ImageMessage(int max_queue_size);

            std::string front_base64_string_message();
        };
    }
}


#endif //ROBOCAR_EC_IMAGE_MSG_H
