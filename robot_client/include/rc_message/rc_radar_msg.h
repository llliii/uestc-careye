//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_RM_RADAR_MSG_H
#define ROBOCAR_RM_RADAR_MSG_H

#include <map>
#include <rc_system/data_struct.h>
#include <boost/thread/mutex.hpp>
#include <rc_message/rc_base_msg.hpp>

namespace rccore {
    namespace message {
        class RadarMessage : public BaseMessage<std::vector<rccore::data_struct::DOT>> {
        public:
            explicit RadarMessage(int max_queue_size);

            ~RadarMessage();
        };
    }
}

#endif //ROBOCAR_RM_RADAR_MSG_H
