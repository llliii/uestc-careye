//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_RM_SERIAL_MSG_H
#define ROBOCAR_RM_SERIAL_MSG_H

#include <rc_system//data_struct.h>
#include <rc_message/rc_base_msg.hpp>
#include <map>

namespace rccore {
    namespace message {

        class SerialMessage : public BaseMessage<std::map<int, rccore::data_struct::rc_SerialPackage>> {
        private:
            bool is_init = false;

        public:

            SerialMessage(int max_queue_size);

            void init(std::string device, int freq);

            int send(int head, int size, char *message);

            int run();

            int recive();
        };
    }
}


#endif //ROBOCAR_RM_SERIAL_MSG_H
