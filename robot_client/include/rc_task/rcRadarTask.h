//
// Created by Pulsar on 2020/5/16.
//

#ifndef UESTC_CAREYE_RCRADARTASK_H
#define UESTC_CAREYE_RCRADARTASK_H

#include <memory>
#include <rc_system/context.h>
#include <rc_task/rcBaseTask.h>

namespace rccore {
    namespace task {
        class RadarTask : public BaseTask {
        public:
            RadarTask(std::shared_ptr<common::Context> pcontext);

            void Run();
        };

        /**
         * 雷达任务
         * @param rcRadarInfo
         */
        void run_radar_task(std::shared_ptr<common::Context> pcontext);
    }
}


#endif //UESTC_CAREYE_RCRADARTASK_H
