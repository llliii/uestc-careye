//
// Created by PulsarV on 18-5-14.
//

#ifndef ROBOCAR_RCTASKMANAGER_H
#define ROBOCAR_RCTASKMANAGER_H

#include <map>
#include <rc_move/rcmove.h>

namespace rccore {
    namespace task {
        class TaskManager {
        public:
            TaskManager(std::shared_ptr<common::Context> _pcontext);

        private:
            std::shared_ptr<common::Context> pcontext;

        };
    }
}
#endif //ROBOCAR_RCTASKMANAGER_H
