//
// Created by PulsarV on 18-5-14.
//

#ifndef ROBOCAR_RCFRAMEQUEUE_H
#define ROBOCAR_RCFRAMEQUEUE_H

#include <opencv2/opencv.hpp>
#include <queue>
#include <rc_system/context.h>
#include <rc_task/rcBaseTask.h>
#include <rc_network/rc_httpd_rest.h>

namespace rccore {
    namespace task {
        class WebStreamTask : public BaseTask {
        public:
            explicit WebStreamTask(std::shared_ptr<common::Context> pcontext);

            void Run() override;

            void Stop() override;

            std::shared_ptr<rccore::network::httpd::RcHttpdRest> prcHttpdRest;
        };

        /**
         * web服务任务
         * @param rcServerInfo
         */
        void run_httpd_task(const std::shared_ptr<common::Context> &pcontext);

        /**
         * websocket服务任务
         * @param rcServerInfo
         */
        void run_websocket_task(std::shared_ptr<common::Context> pcontext);
    }
}
#endif //ROBOCAR_RCFRAMEQUEUE_H
