function button_ac_click() {
    send_command('/move/ac/30')
}

function button_cw_click() {
    send_command('/move/cw/30')
}

function button_control_click() {
    send_command('/move/control')
}

function button_back_click() {
    send_command('/move/back/0.5')
}

function button_front_click() {
    send_command('/move/front/0.5')
}

function button_left_click() {
    send_command('/move/left/0.5')
}

function button_right_click() {
    send_command('/move/right/0.5')
}

function button_takeoff_click() {
    send_command('/move/takeoff/0')
}

function button_stop_click() {
    send_command('/move/stop/0')
}

function button_up_click() {
    send_command('/move/up/0.5')
}

function button_down_click() {
    send_command('/move/down/0.5')
}

function button_record_click(object) {
    console.log(object.innerHTML);
    $.ajax({
        type: "POST",
        url: "/command/recoard",
        dataType: "JSON",
        success: function (result) {
            console.log(result);
            if (result["value"])
                object.innerHTML = "停止";

            else
                object.innerHTML = "录制";
        },
        error: function (e) {
            console.log(e.status);
            console.log(e.responseText);
        }
    });
}

function send_command(urlpath) {
    $.ajax({
        type: "POST",
        url: urlpath,
        dataType: "JSON",
        success: function (result) {
            console.log(result);
        },
        error: function (e) {
            console.log(e.status);
            console.log(e.responseText);
        }
    });
}


function set_params(urlpath, parama_name, value) {
    $.ajax({
        type: "POST",
        url: urlpath + parama_name,
        dataType: "JSON",
        success: function (result) {
            console.log(result);
        },
        error: function (e) {
            console.log(e.status);
            console.log(e.responseText);
        }
    })
}

