//
// Created by Pulsar on 2020/5/16.
//
#include <rc_task/rcRadarTask.h>
#include <iostream>
#include <signal.h>
#include <boost/thread/thread.hpp>
using namespace RC::Task;

bool ctrl_c_pressed;

void ctrlc(int) {
    ctrl_c_pressed = true;
    std::cout<<"Exit"<<std::endl;
}

int main(int argc, char **argv) {
    signal(SIGINT, ctrlc);
    rc_RadarInfo rcRadarInfo;
    rcRadarInfo.radar_device=(char*)"/dev/ttyUSB0";
    boost::thread robot_radar_module(boost::bind(run_radar_task, rcRadarInfo));
//    run_radar_task(rcRadarInfo);
    getchar();
    robot_radar_module.interrupt();
    robot_radar_module.join();
    getchar();
    return 1;
}