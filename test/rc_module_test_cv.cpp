//
// Created by Pulsar on 2020/5/16.
//
#include <rc_system/config.h>
#include <rc_system/context.h>
#include <rc_task/rcCVTask.h>
#include <rc_task/rcTaskVariable.h>
#include <rc_log/slog.hpp>

std::atomic<bool> flag = true;

void sig_handler(int signum) {
    if (signum == SIGINT) {
        if (flag) {
            std::cout << "Exiting..." << std::endl;
            flag = false;
        } else {
            exit(0);
        }
    }
}

int main(int argc, char **argv) {
    signal(SIGINT, sig_handler);
    std::shared_ptr<rccore::common::Config> pconfig = std::make_shared<rccore::common::Config>("config.yml");
    if (pconfig->Load())
        pconfig->Load();
    std::shared_ptr<rccore::common::Context> pcontext = std::make_shared<rccore::common::Context>(pconfig);
    pcontext->Init();

    rccore::task::CvTask cv_task(pcontext);
    cv_task.Run();
    while (flag) {}
    cv_task.Stop();
    while (rccore::task::task_variable::TASK_NUM) {};
    return 1;
}